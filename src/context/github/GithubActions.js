const GITHUB_URL = process.env.REACT_APP_GITHUB_URL

// Get initial state 
export const searchUsers = async (text) => {

    const params = new URLSearchParams({
      q: text
    })

    const response = await fetch(`${GITHUB_URL}/search/users?${params}`)
    const {items} = await response.json() // {items} performs destructing json to the object that i want

    return items
  }

    // Get initial state
    export const getUser = async (login) => {

        const response = await fetch(`${GITHUB_URL}/users/${login}`)
  
        if(response.status === 404){
          window.location = "/notfound"
        }else { 
  
          const item = await response.json() // {items} performs destructing json to the object that i want
          return item
      }
    }
  
    
    // Get user repos
    export const getUserRepos = async (login) => {

        const params = new URLSearchParams({
          sort: "created",
          per_page: 10
        })
  
        const response = await fetch(`${GITHUB_URL}/users/${login}/repos?${params}`)
        const data = await response.json() // {items} performs destructing json to the object that i want
  
        return data
    }