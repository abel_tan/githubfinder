import { createContext, useReducer } from "react";
import { createRenderer } from "react-dom/test-utils";
import githubReducer from "./GithubReducer";

const GithubContext = createContext()

export const GithubProvider = ({children}) => {
    const initialState = {
        users: [],
        user: {},
        repos: [],
        loading: false
    }

    // useReducer => to dispatch an action to githubReducer for modification of the state (state, action)
    const [state, dispatch] = useReducer(githubReducer, initialState)
    

  return <GithubContext.Provider value={{
    ...state,
    dispatch
  }}>
    {children}
  </GithubContext.Provider>
}

export default GithubContext