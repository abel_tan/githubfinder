import {BrowserRouter as Router, Routes, Route } from 'react-router-dom'
import NavBar from './components/layout/NavBar';
import Footer from './components/layout/Footer';
import Alert from './components/Alert';
import Home from './pages/Home';
import About from './pages/About';
import User from './pages/User';
import NotFound from './pages/NotFound';
import { GithubProvider } from './context/github/GithubContext';
import { AlertProvider } from './context/alert/AlertContext';


function App() {
  return (
    <GithubProvider>
      <AlertProvider>
        <Router>
          <div className='app'>
            <NavBar />
            <main className='container mx-auto px-3 pb-12'>
                <Alert />
                <Routes>
                  <Route exact path='/' element={<Home />} />
                  <Route path='/about' element={<About />} />
                  <Route path='/user/:login' element={<User />} />
                  <Route path='/notfound' element={<NotFound />} />
                  <Route path='/*' element={<NotFound />} />
                </Routes>
            </main>
          </div>
        </Router>
      </AlertProvider>
    </GithubProvider>
    
  );
}

export default App;
