import { useContext} from 'react'
import Spinner from "../layout/Spinner"
import UserItem from './UserItem'
import GithubContext from '../../context/github/GithubContext'

function UserResults() {
  const {users, loading} = useContext(GithubContext)

  return (!loading? (
    <div className='grid grid-cols-1 gap-8 xl:gri-cols-4 
    lg:gri-cols-3 md:grid-cols-2'>
      {users.map((user) => (
        // <h3>{user.login}</h3>  
        <UserItem key={user.id} user={user} />
      ))}
    </div>
  ):(
    <Spinner />
  ))
    
}

export default UserResults